<?php if ($numRowBottomModules > 0):?>
    <div class="modules-main" id="modulesBottom">
        <?php
            $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
            
            $links = array();
            
            for ($i = 0; $i < 3; $i++) {
                $tmp = get_module($outRowBottomModules[$i]['mod_name']);
                
                preg_match($href, $tmp, $m);

                if ($m[2] != '') {
                    $links[] = $m[2];
                } else {
                    $links[] = trans_url_name($outRowBottomModules[$i]['name']);
                }
            }
        ?>
        <ul>
            <?php for ($i = 0; $i < $numRowBottomModules; $i++): ?>
                <li>
                    <div id="<?php echo $outRowBottomModules[$i]['mod_name']; ?>">
                        <h2>
                            <?php echo $outRowBottomModules[$i]['name']; ?>    
                        </h2>
                        <?php echo get_module($outRowBottomModules[$i]['mod_name']); ?>
                    </div>
                </li>
            <?php endfor; ?>
        </ul>
    </div>
<?php endif; ?>