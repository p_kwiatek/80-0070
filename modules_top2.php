<?php if ($numRowTop2Modules > 0):?>
    <div class="modules-main" id="modulesTop2">
        <?php
            $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
            
            $links = array();
            
            for ($i = 0; $i < 3; $i++) {
                $tmp = get_module($outRowTop2Modules[$i]['mod_name']);
                
                preg_match($href, $tmp, $m);

                if ($m[2] != '') {
                    $links[] = $m[2];
                } else {
                    $links[] = trans_url_name($outRowTop2Modules[$i]['name']);
                }
            }
        ?>
        <ul> 
            <?php for ($i = 0; $i < $numRowTop2Modules; $i++): ?>
                <li>
                	<div id="<?php echo $outRowTop2Modules[$i]['mod_name']; ?>">
                        <h2>
                            <?php echo $outRowTop2Modules[$i]['name']; ?>    
                        </h2>
                        <?php echo get_module($outRowTop2Modules[$i]['mod_name']); ?>
                    </div>
                </li>
            <?php endfor; ?>
        </ul>
    </div>
<?php endif; ?>