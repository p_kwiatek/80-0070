<aside id="menuCol" class="aside hidden-xs hidden-sm">
    <div class="aside__menu" id="skip_mg">
    	<h2 class="heading"><?php echo __('menu'); ?></h2>
        <?php
			get_menu_tree ('mg', 0, 0, '', false, '', false, true, true);    
		?>
		<div class="shTrunk"></div>
		<?php
	        /*
			 * Dynamic menus
			 */
			foreach ($menuType as $dynMenu)
			{
			    if ($dynMenu['menutype'] != 'mg' && $dynMenu['menutype'] != 'tm')
			    {
					if ($dynMenu['active'] == 1)
					{
					    
					    $menuClass = '';
					    if (strlen($dynMenu['name']) > 30)
					    {
						$menuClass = ' menuHead3';
					    } else if (strlen($dynMenu['name']) > 17 && strlen($dynMenu['name']) <= 30)
					    {
						$menuClass = ' menuHead2';
					    } else
					    {
						$menuClass = ' menuHead1';
					    }
					    ?>
					    <h2 class="heading"><?php echo $dynMenu['name']?></h2>
					    <?php
					    get_menu_tree ($dynMenu['menutype'], 0, 0, '', false, '', false, true, true);
					    ?>
					    <?php
					} 
			    }
			}
		?>
    </div>
    <div id="modulesLeftWrapper" class="aside__modules">
    <?php
    $i = 0;
    foreach ($outRowLeftModules as $module)
    {
	$i++;
	$mod = ' module'.$i;
	$type = 'Short';
	if ($module['mod_name'] == 'mod_kzk')
	{
	    $type = 'Long';
	}		
	?>
	<div id="<?php echo $module['mod_name']?>" class="moduleLeft module<?php echo $mod?>">    
		<h2 class="moduleLeftHead<?php echo $type?>"><span><?php echo $module['name']?></span></h2>
	    <div class="module_icon"></div>
	    <div class="module_content"><?php echo get_module ($module['mod_name'])?></div>
	</div>
	<?php
	if ($i == 2)
	{
	    $i = 0;
	}
    }
    ?>
    </div>
    
    <?php
    if (count($leftAdv) > 0)
    {
	?>
	<div id="advertsLeftWrapper">
	    <div id="advertsLeftTop"></div>
	    <div id="advertsLeftContent">
	<?php
	$n = 0;
	foreach ($leftAdv as $adv)
	{
	    ?>
	    <div class="advertLeft">
	    <?php
	    $extUrl = '';
	    if ($adv['ext_url'] != '')
	    {
		$newWindow = '';
		if ($adv['new_window'] == 1)
		{
		    $newWindow = ' target="_blank" ';
		}
	    			
		$swfLink = '';
		$swfSize = '';
		if ($adv['attrib'] != '' && $adv['type'] == 'flash')
		{
		    //$swfLink = ' style="width:'.$swfSize[0].'px; height:'.$swfSize[1].'px; z-index:1; display:block; position:absolute; left:0; top:0" ';
		}
					
		$extUrl = '<a href="'.$adv['ext_url'].'"'.$newWindow.$swfLink.'>';
		$extUrlEnd = '</a>';
	    }
				
	    switch ($adv['type'])
	    {
		case 'flash':
		    $swfSize = explode(',', $adv['attrib']);
		    //echo $extUrl;
		    ?>
		    <div id="advLeft_<?php echo $n?>"><?php echo $adv['attrib']?></div>
		    <script type="text/javascript">
		    // <![CDATA['
		    swfobject.embedSWF("<?php echo $adv['content'] . '?noc=' . time()?>", "advLeft_<?php echo $n?>", "<?php echo $swfSize[0]?>", "<?php echo $swfSize[1]?>", "9.0.0", "expressInstall.swf", {}, {wmode:"transparent"}, {});
		    // ]]>'
		    </script>
		    <?php			
		    //echo $extUrlEnd;
		    break;
					
		case 'html':
		    echo $adv['content'];
		    break;
					
		case 'image':
		default:
		    $width = '';
		    if ($adv['content'] != '')
		    {                    
                        $size = getimagesize($adv['content']);
                        if ($size[0] > $templateConfig['maxWidthLeftAdv'])
                        {
                            $width = ' width="'.$templateConfig['maxWidthLeftAdv'].'" ';
                        } 
                        echo $extUrl;
                        ?>
                        <img src="<?php echo $adv['content']?>" alt="<?php echo $adv['name']?>" <?php echo $width?>/>
                        <?php
                        echo $extUrlEnd;
		    }
		    break;
	    }
	    ?>
	    </div>
	    <?php
	    $n++;
	}
	?>
	    </div>
	    
	</div>
	<div id="advertsLeftBottom"></div>
	<div class="shTrunk2"></div>
	<div id="mod_programs-desktop"></div>
	<?php
    }
    ?>

</aside>