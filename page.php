<h2><?php echo $pageName ?></h2>
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
	
if ($showPage)
{
    echo $row['text'];
		
    if (! check_html_text($row['author'], '') )
    {
	?>
	<div class="authorName"><?php echo __('author'); ?>: <span><?php echo $row['author']?></span></div>
	<?php
    }
		
    /*
     * Articles
     */
    if ($numArticles > 0)
    {	
	$i = 0;
	$articleWrapper = '';
	if ($row['text'] != '')
	{
	    $articleWrapper = ' class="articleWrapper"';
	}
	?>		
	<div<?php echo $articleWrapper?>>
	    <h3 class="sr-only"><?php echo __('articles'); ?></h3>
	    <?php
	    foreach ($outRowArticles as $row)
            {
		$highlight = $url = $target = $url_title = $protect = '';
			
		if ($row['protected'] == 1)
		{
		    $protect = '<span class="protectedPage"></span>';
		    $url_title = ' title="' . __('page requires login') . '"';
		}				
				
		if (trim($row['ext_url']) != '')
		{
		    if ($row['new_window'] == '1')
		    {
			$target = ' target="_blank"';
		    }	
		    $url_title = ' title="' . __('opens in new window') . '"';
		    $url = ref_replace($row['ext_url']);					
		} else
		{
		    if ($row['url_name'] != '')
		    {
			$url = 'art,' . $row['id_art'] . ',' . $row['url_name'];
		    } else
		    {
			$url = 'index.php?c=article&amp;id=' . $row['id_art'];
		    }
		}	
				
		$margin = ' noPhoto';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $margin = '';
		}				
				
		$row['show_date'] = substr($row['show_date'], 0, 10);
		
		$highlight = '';
		$frame = 'normalFrame';
		$photoWrapper = '';
		if ($row['highlight'] == 1)
		{
		    $highlight = ' article--highlighted';
		    $frame = 'highlightFrame';
		    $photoWrapper = ' highlightWrapper';
		}			
		?>
	    <div class="article<?php echo $highlight?>">
		<?php
		$marginPhoto = '';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $photo = $photoLead[$row['id_art']];
		    $marginPhoto = ' paddingBottom';
		    ?>
			<a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" class="article__image">
				<div><img src="files/<?php echo $lang?>/mini/<?php echo $photo['file']?>" alt="<?php echo __('enlarge image'); ?>"></div>
			</a> 
		    <?php
		}
		?>
			<div class="article__content">
				<h4><a href="<?php echo $url?>" <?php echo $url_title . $target?>><?php echo $row['name'] . $protect ?></a></h4>
				<?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
				    <div class="article__date"><span><?php echo $row['show_date']?></span></div>
				<?php } ?>
			    <div><?php echo $row['lead_text']?></div>
			    <a href="<?php echo $url?>" <?php echo $url_title . $target?> class="article__link"><?php echo __('read more'); ?><span class="sr-only"> <?php echo __('about'); ?>: <?php echo $row['name'] . $protect?></span></a>
			</div>
		</div>
		<?php		
	    }
	    ?>
	    	
	    <?php

	$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;id=' . $_GET['id'] . '&amp;s=';
	include (CMS_TEMPL . DS . 'pagination.php');
	?>
	</div>
	<?php
    }			
				
	/*
	 * Wypisanie plikow do pobrania
	 */
    if ($numFiles > 0)
    {	
    ?>
	<div class="filesWrapper">
	    <h3 class="filesHead"><?php echo __('files'); ?></h3>
	    <ul>
	    <?php
	    foreach ($outRowFiles as $row)
            {
		$target = 'target="_blank" ';
				
		if (filesize('download/'.$row['file']) > 5000000)
		{
		    $url = 'download/'.$row['file'];
		} else
		{
		    $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
		}
		if (trim($row['name']) == '')
		{
		    $name = $row['file'];
		} else
		{
		    $name = $row['name'];
		}
					
		$size = file_size('download/'.$row['file']);	
		?>		
		<li>
		    <h4>
			<a href="<?php echo $url?>" <?php echo $target?>><?php echo $name?></a> <span>(<?php echo $size?>)</span>
		    </h4>
		</li>
		<?php
	    }
	    ?>
	    </ul>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<ul class="gallery">
	    <?php
	    foreach ($outRowPhotos as $row)
	    {
		$i++;
		$noMargin = '';
		if ($i == $pageConfig['zawijaj'])
		{
		    $noMargin = ' noMargin';
		}
		?>
		<li class="photoWrapperGallery<?php echo $noMargin?>">
		    <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" data-fancybox-group="gallery" rel="fancybox" title="<?php echo $row['name']?>" class="photo"><img src="files/<?php echo $lang?>/mini/<?php echo $row['file']?>" alt="<?php echo __('enlarge image'); ?>" /></a>
		    <?php
		    if (! check_html_text($row['name'], '') ) {
			?>
			<p><?php echo $row['name']?></p>
			<?php
		    }
		    ?>
		</li>
		<?php
		if ($i == $pageConfig['zawijaj'])
		{
		    $i = 0;
		    ?>
		    <?php
		}						
	    }
	    ?>
	</ul>
	<?php
    }		

    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=page&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
	}
}
?>
