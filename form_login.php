<form action="index.php?c=<?php echo $_GET['c'].'&amp;id='.$_GET['id']; ?>" method="post">
	<?php 

		// Potrzebne do sprawdzenia podczas logowania czy user ma uprawnienia do stron chronionych i forum
	
		switch ($_GET['c']) {
			case 'page' : 
			case 'article' : 
					$pValue = "text"; 
					break;
			case 'modules' : 
					$pValue = "forum"; 
					break;
		}
	?>
    <input type="hidden" name="protect" value="<?php echo $pValue; ?>" />
    
	<div class="form">
	    	<h3><?php echo __('login action'); ?></h3>
		<div class="group">
		    <label for="fLogin" class="formLabel"><?php echo __('login'); ?></label>
		    <input type="text" class="inText" name="fLogin" id="fLogin" value="" />
		    <span id="loginMsg" class="msgMarg"></span>
		</div>
		<div class="group">
		    <label for="fPass" class="formLabel"><?php echo __('password'); ?>:</label>
		    <input type="password" class="inText" name="fPass" id="fPass" value="" />
		    <span id="passwordMsg" class="msgMarg"></span>
		</div>      
		<div class="group">
		    <input type="submit" name="loginUser" value="<?php echo __('login action'); ?>" class="btnForm"/>   
		</div>
		<div class="group">
			<a href="generuj-haslo" class="buttonMedium"><?php echo __('password forgot'); ?></a>
		</div>
    </div>
</form>

<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	$('#fLogin').blur(validateLogin);
	function validateLogin(){
	    if ($('#fLogin').val().length < 4){
		$('#fLogin').addClass('inError');
		$('#loginMsg').addClass('msgError').text('<?php echo __('error min length login'); ?>');
		return false;
	    } else {
		$('#fLogin').removeClass('inError');
		$('#loginMsg').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#fPass').blur(validatePassword);
	function validatePassword(){
	    if ($('#fPass').val().length < 8){
		$('#fPass').addClass('inError');
		$('#passwordMsg').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		return false;
	    } else {
		$('#fPass').removeClass('inError');
		$('#passwordMsg').removeClass('msgError').text('');		
		return true;
	    }
	}
	
    });
// ]]>	
</script>