<div class="main-footer">
    <div class="container">
    	<div class="row">
    		<div class="col-md-10 hidden-mobile">
                <?php get_menu_tree('tm', 0, 0, '', true, 'bm', true); ?>
                <?php include_once ( CMS_TEMPL . DS . 'bottom.php'); ?>      
            </div>
    		<div class="col-md-2">
    			<a href="#top" id="goTop"><?php echo __('go to top'); ?></a>
    		</div>
    	</div>
    </div>
</div>
<div class="footer--copyright">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="mapa_strony" id="skip_map" class="footer__sitemap"><?php echo __('sitemap') ?></a>
                <p>
                    <?php echo __('designed'); ?>:
                    <a href="http://szkolnastrona.pl">Szkolnastrona.pl</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php
/*
 * Pobranie z zewnątrz
 */
echo get_url_content($external_text['wwwStart'], 'wwwStart', true);
?>
</body>
</html>