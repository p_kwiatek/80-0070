<?php
if ($showForgotForm){
?>
<a id="wyslij"></a>
<h2><?php echo $pageName; ?></h2>
<form name="forgotForm" id="forgotForm" class="" method="post" action="<?php echo $url; ?>,nowe-haslo#wyslij">
    <div class="form">
		<?php
		echo $message;
		?>
		<h3><?php echo __('new password'); ?></h3>
		
		<div class="group">
			<p><?php echo __('generate new password info'); ?></p>
		</div>
		
		<div class="group">
			<label for="email"><?php echo __('email'); ?>:</label>
			<input type="text" id="email" name="email" class="inText" size="35" maxlength="50" value="<?php echo $email; ?>" />
			<span id="emailError" class="msgMarg"></span>
		</div>

		<div class="group">
			<input type="submit" name="ok" value="<?php echo __('send'); ?>" />
		</div>
	
    </div>
</form>

<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	var form = $('#forgotForm');
	form.submit(function(){
	    if (validateEmail()){
		//return true;
	    } else {
		//return false;
	    }
	});
	
	$('#email').blur(validateEmail);
	function validateEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#email").val();
	    if (!exp.test(email)){
		$('#email').addClass('inError');
		$('#emailError').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#email').removeClass('inError');
		$('#emailError').removeClass('msgError').text('');
		return true;
	    }
	}	
    });
// ]]>	
</script>

<?php
}
if ($showForgotInfo){
?>

<h2><?php echo $pageName; ?></h2>
<?php
echo $message;
?>
<ul id="backLinks">
    <li><a href="forum" class="button"><?php echo __('forum home page'); ?></a></li>
    <li><a href="index.php" class="button"><?php echo __('home page'); ?></a></li>
</ul>
<?php
}
?>

