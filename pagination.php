<?php
if ($pagination['end'] > 1)
{
    ?>
    <div class="pagination">
    <?php
    $active_page = $pagination['active'];
		
    if ( !isset($pagination['active']) )
    {
	$active_page = 1;
    }
    ?>
    <p><?php echo __('page'); ?> <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
    <ul>
    <?php
    if ($pagination['start'] != $pagination['prev'])
    {
	?>
	<li><a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btnStart"><span class="sr-only"><?php echo __('first page'); ?> </span>&lt;&lt;</a></li>
	<li><a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btnPrev"><span class="sr-only"><?php echo __('prev page'); ?> </span>&lt;</a></li>
	<?php
    }
		
    foreach ($pagination as $k => $v)
    {
	if (is_numeric($k))
	{
	    ?>
	    <li><a href="<?php echo $url . $v?>" rel="nofollow" class="btnPage"><span class="sr-only"><?php echo __('page'); ?> </span><?php echo $v?></a></li>
	    <?php
	} else if ($k == 'active')
	{
	    ?>
	    <li class="pageActive"><span class="sr-only"><?php echo __('page'); ?> </span><?php echo $v?></li>
	    <?php
	}			
    }
		
    if ($pagination['active'] != $pagination['end'])
    {
	?>
	<li><a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btnNext"><span class="sr-only"><?php echo __('next page'); ?> </span>&gt;</a></li>
	<li><a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btnEnd"><span class="sr-only"><?php echo __('last page'); ?> </span>&gt;&gt;</a></li>
	<?php
    }
    ?>
    </ul>
    </div>
    <?php
}
?>