(function($) {
	'use strict';

	var project = {};

	project.focusLink = function() {
		var a = new Date,
	        b = new Date;
	    $(document).on("focusin", function(c) {
	        $(".keyboard-outline").removeClass("keyboard-outline");
	        var d = b < a;
	        d && $(c.target).addClass("keyboard-outline");
	    }), $(document).on("click", function() {
	        b = new Date;
	    }), $(document).on("keydown", function() {
	        a = new Date;
	    });
	}
	
	var isMobileView = false;

	project.startUI = function() {
		$('#advertsTopWrapper').wrap('<div id="advertsTopWrapper-desktop"></div>');
        $('#content_txt').wrap('<div id="content_txt-desktop"></div>');
    	$('.aside__menu').wrap('<div id="menuLeft-desktop"></div>');
        $('.headmodules').wrap('<div id="headmodules-desktop"></div>');
        $('#modulesTop2').wrap('<div id="modulesMainTop-desktop"></div>');
        $('#modulesLeftWrapper').wrap('<div id="modulesLeftWrapper-desktop"></div>');
	}

	project.updateUI = function() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            $('#advertsTopWrapper').prependTo('#advertsTopWrapper-mobile');
            $('#content_txt').prependTo('#content_txt-mobile');
            $('.aside__menu').prependTo('#menuLeft-mobile');
            $('.headmodules').prependTo('#headmodules-mobile');
            $('#modulesTop2').prependTo('#modulesMainTop-mobile');
            
            $('#modulesLeftWrapper').prependTo('#modulesLeftWrapper-mobile');
            $('#mod_programs').appendTo('#modPrograms-mobile .aside__modules');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('#advertsTopWrapper').prependTo('#advertsTopWrapper-desktop');
            $('#content_txt').prependTo('#content_txt-desktop');
            $('.aside__menu').prependTo('#menuLeft-desktop');
            $('.headmodules').prependTo('#headmodules-desktop');
            $('#modulesTop2').prependTo('#modulesMainTop-desktop');
            
            $('#modulesLeftWrapper').prependTo('#modulesLeftWrapper-desktop');
            $('#mod_programs').appendTo('#modulesLeftWrapper-desktop .aside__modules');
        }
		if (Modernizr.mq('(max-width: 991px)')) {
			$('html').css({'font-size':'62.5%'});
		}
    }

	project.fancybox = function() {
		$('.photoWrapperGallery a').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	300, 
			'speedOut'		:	300, 
			'overlayShow'	:	true
		});
		$('.article__image').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	300, 
			'speedOut'		:	300, 
			'overlayShow'	:	true
		});
		if (popup.show) {
			$.fancybox(
			    popup.content
			);	
	    }
	}

	project.comeBack = function() {
		$('#goTop').on('click', function(e) {
			e.preventDefault();
			$('html,body').animate({ scrollTop: 0 }, 'slow');
        	return false; 
		});
	}

	project.nav = function() {
		$('[role="menu"]').closest('li').addClass('menu__holder');
		$('body').on('click','.menu__holder > a:not(.ready)', function(e) {
			e.preventDefault();
			$('.main-nav .menu__holder > .ready').removeClass('ready');
			$('.main-nav .dropdown-menu').removeClass('active');
			$(this).addClass('ready');
			$(this).siblings('.dropdown-menu').addClass('active');
		});

		$('body').on('click','.menu__holder > a.ready','click', function() {
			var href = $(this).attr('href');
			window.location.href = href;
		});

		$(document).on('click', function(e) {
			var target = e.target;
			if (!$(target).is('.dropdown-menu') && !$(target).is('.main-nav .menu__holder > a')) {
				$('.dropdown-menu').removeClass('active');
				$('.main-nav .menu__holder > .ready').removeClass('ready');
			}
		});
	}

	project.menu = function() {
		$('.nav__hamburger').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
			$('.nav__menu').toggleClass('active');
		});
	}

	project.search = function() {
		$('.nav__search').on('click', function() {
			$(this).addClass('active');
		});
	}

	project.modulesMain = function() {
		$('.modules-main > ul > li').each(function() {
			if ($(this).find('a').length) {
				var href = $(this).find('a').attr('href');
				$(this).css({'cursor':'pointer'});
				$(this).on('click', function() {
					window.location.href = href;
				});
			}
			if (!$(this).children('div').attr('id').length) {
				$(this).hide().addClass('no-count');
			}
		});
		$('.modules-main').each(function() {
			if ($(this).children('ul').children('li:not(.no-count)').length == 3) {
				$(this).children('ul').children('li:not(.no-count)').addClass('three');
			}
			else if ($(this).children('ul').children('li:not(.no-count)').length == 2) {
				$(this).children('ul').children('li:not(.no-count)').addClass('two');
			}
			else if ($(this).children('ul').children('li:not(.no-count)').length == 1) {
				$(this).children('ul').children('li:not(.no-count)').addClass('one');
			}
		});
	}

	project.triggerUpload = function() {
		$('#btnFilePos').on('click', function() {
			$('#avatar_f').trigger('click');
		});
	}

	project.calendar = function() {
		$(document).on("click", ".caption_nav_prev a", function()
		{ 
		    var date = $(this).attr('id').substr(2);
		    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

		    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
		});

		$(document).on("click", ".caption_nav_next a", function()
		{ 
		    var date = $(this).attr('id').substr(2);
		    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

		    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);		
		});
	}

	project.menuLeft = function() {
		$('.aside__menu ul[role="menu"]').siblings('a').addClass('not-ready');
		$('body').on('click','.not-ready','click', function(e) {
			e.preventDefault();
			$(this).addClass('ready').removeClass('not-ready');
			$(this).siblings('ul').show();
		});

		$('.menu--left .menu__holder').on('click', function() {
			$(this).children('ul').show();
		});

		var parent = $('.aside a.selected').closest('#mg > .menu__holder');
		var parentDropdowns = $(parent).find('.dropdown-menu');
		$(parentDropdowns).show();
	}

	project.weather = function() {
		$('.weatherParam').clone().appendTo('#mod_weather .module_content #weatherInfo').addClass('cloned');
	}

	project.questionnaire = function() {
		$('.qBar').wrap('<div class="question-bar-wrapper" style="width: calc(100% - 80px);"></div>')
	}

	project.articles = function() {
		$('.article').each(function() {
			$(this).height($(this).children('.article__content').outerHeight(true));
		})
	}

	project.contact = function() {
		$('#mod_contact p').each(function() {
			var text = $(this).text();
			if (text.indexOf('mail') >= 0) {
				$(this).addClass('mail');
			}
		});
	}

	project.init = function() {
		project.focusLink();
		project.startUI();
		project.fancybox();
		project.comeBack();
		project.nav();
		project.menu();
		project.search();
		project.modulesMain();
		project.triggerUpload();
		project.calendar();
		project.menuLeft();
		project.weather();
		project.questionnaire();
		project.contact();
	}

	var calcModulesHeight = 0;
	$(window).on('load', function() {
		project.updateUI();
		project.articles();
		if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {
			var maxHeight = 0;
			if (calcModulesHeight == 0) {
				calcModulesHeight = 1;
				$('#modulesLeftWrapper > *').each(function(){
				    var thisH = $(this).outerHeight();
				    if (thisH > maxHeight) { 
				   		maxHeight = thisH; 
				    }
				});
				$('#modulesLeftWrapper > *').height(maxHeight);
			}	
		}
	});

	$(window).on('resize', function() {
		project.updateUI();
		project.articles();
		if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {
			var maxHeight = 0;
			if (calcModulesHeight == 0) {
				calcModulesHeight = 1;
				$('#modulesLeftWrapper > *').each(function(){
				    var thisH = $(this).outerHeight();
				    if (thisH > maxHeight) { 
				   		maxHeight = thisH; 
				    }
				});
				$('#modulesLeftWrapper > *').height(maxHeight);
			}	
		}
	});

	$(document).ready(project.init);

})(jQuery);