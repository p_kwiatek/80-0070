<?php
if (count($outRowTopModules) > 0)
{
?>
    <section class="headmodules">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ul class="headmodules__holder">
						<?php
						$i = 0;
						foreach ($outRowTopModules as $module)
						{
						    $i++;
						    $no_margin = '';
						    if ($i == 3)
						    {
							$no_margin = ' noMargin';						
						    }

						    $mod = ' module'.$i;
						    $type = 'Short';
						    if ($module['mod_name'] == 'mod_kzk')
						    {
							$type = 'Long';
						    }
						    ?>
						    <li id="<?php echo $module['mod_name']?>" class="<?php echo $mod?>">
							    <div class="module_icon"></div>
							    <div class="module_content">
							   		<h2 class="moduleTopHead<?php echo $i . $type?>"><?php echo $module['name']?></h2>
							    	<?php echo get_module($module['mod_name'], 'top') ?>	
						    	</div>
						    </li>
						    <?php
						}
						?>
					</ul>
				</div>
			</div>
		</div>
    </section>
    <?php
}
?>