<h2 class="heading"><?php echo $pageName; ?></h2>
<?php
	// Wypisanie artykulow
	if ($numArticles > 0)
	{	
		$i = 0;
		echo '<section class="articles">';
		// echo '<h2 class="sr-only">' . __('articles') . '</h2>';
		foreach ($outArticles as $row)
		{
			$highlight = $url = $target = $url_title = $protect = '';
			
			if ($row['protected'] == 1)
			{
				$protect = '<span class="protectedPage"></span>';
				$url_title = ' title="' . __('page requires login') . '"';
			}				
			
			if (trim($row['ext_url']) != '')
			{
				if ($row['new_window'] == '1')
				{
					$target = ' target="_blank"';
				}	
				$url_title = ' title="' . __('opens in new window') . '"';
				$url = ref_replace($row['ext_url']);					
			}
			else
			{
			    if ($row['url_name'] != '')
			    {
				$url = 'a,' . $row['id_art'] . ',' . $row['url_name'];
			    } else
			    {
				$url = 'index.php?c=article&amp;id=' . $row['id_art'];
			    }
			}	
						
			if ($row['highlight'] == 1) {
				$highlight = ' article--highlighted';
			}
			
			$margin = ' noPhoto';
			if (is_array($photoLead[$row['id_art']]))
			{
			    $margin = '';
			}			
			
			$row['show_date'] = substr($row['show_date'], 0, 10);
			
			echo '<div class="article' . $highlight . '">';
			if (is_array($photoLead[$row['id_art']]))
			{
				$photo = $photoLead[$row['id_art']];
				?>
				<a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" class="article__image">
					<div><img src="files/<?php echo $lang?>/mini/<?php echo $photo['file']?>" alt="<?php echo __('enlarge image'); ?>"></div>
				</a>
				<?php  
			}
			if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') {
				echo '<div class="article__date"><span>'.$row['show_date'].'</span></div>';
			}
			
			echo '<div class="article__content">
				<h4 class="' . $margin . '"><a href="' . $url . '" ' . $url_title . $target .'>' . $row['name'] . $protect . '</a></h4><div>'.$row['lead_text'].'</div>
				<a href="' . $url . '" ' . $url_title . $target .' class="article__link">' . __('read more') . ' <span class="sr-only">' . __('about') . $row["name"] . $protect . '</span></a></div>'
				.'</div>';		
		}


		echo '</section>';			
	}

?>
